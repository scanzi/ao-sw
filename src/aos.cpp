/*
*  C++ Implementation: swift
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>

#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>

#include "constants.h"
#include "sys_util.h"


static void application_init ( void );
static void application_end ( void );
void application ( int argc, char *argv[] );



#include "ao_parameters.h"
#include "wfs_fe.h"
#include "preprocessing.h"
#include "modal_decomposition.h"
#include "optimal_controls.h"
#include "actuators_command.h"

#ifdef USE_FLOAT

float image[IMAGE_SIZE][IMAGE_SIZE];
float slopes[N_SIZE];
//float d[N_SIZE][D_SIZE];
float r[D_SIZE][A_SIZE];
float m[M_SIZE][M_SIZE];
float a[A_SIZE];
float b[B_SIZE];
float c[C_SIZE];
float c_out[C_SIZE];

float d[N_SIZE][D_SIZE] = 
{
    #include "d.h"
};

#else

int image[IMAGE_SIZE][IMAGE_SIZE];
int slopes[N_SIZE];
int d[N_SIZE][D_SIZE];
int r[D_SIZE][A_SIZE];
int m[M_SIZE][M_SIZE];
int a[A_SIZE];
int b[B_SIZE];
int c[C_SIZE];
int c_out[C_SIZE];

#endif

void aos ( uint8_t module, uint8_t mode )
{
    uint32_t tec = 0;
    
    
#if 1   
    switch ( module )
    {

        case PREPROCESSING:
            printf ("--------------------------\n\r");
            tec += preprocessing ( mode );
        break;
        case MODAL_DECOMPOSITION:
            printf ("--------------------------\n\r");
            tec += modal_decomposition ( mode );
        break;
        case OPTIMAL_CONTROLS:
            printf ("--------------------------\n\r");
            tec += optimal_controls( mode );
        break;
        case ACTUATORS_COMMAND:
            printf ("--------------------------\n\r");
            tec += actuators_command( mode );
        break;       
        case ALL:
            printf ("--------------------------\n\r");
            tec += preprocessing ( mode );
            printf ("--------------------------\n\r");
            tec += modal_decomposition ( mode );
            printf ("--------------------------\n\r");
            tec += optimal_controls( mode );
            printf ("--------------------------\n\r");
            tec += actuators_command( mode );
        break;       
            
    }    
    
    printf ("==========================\n\r");
    printf ("Total Execution Time [us]: %d \n\r", tec );

#endif  
}

void application_init ( void )
{

    //printf ("\n\rApplication Starting \n\r" );
    //printf ( "%s", INIT_STRING );
    cout << INIT_STRING;

    signal_handler();

    core_dump_init();
        
    //assign_cpu( ARM_CPU_0 );

#ifdef USE_PL_MEMORY
    fpga_ddr_memory_init ( PL_MEMORY_START, 256*1024*1024 );
#endif  

    hw_accelerators_init ();
    application_run = TRUE;

}

void application_end ()
{
    //printf ("Application Ending \n\r" );

}

#define FILENAME "WFS_image_1.csv"

void application ( int argc, char *argv[] )
{
    uint8_t module = ALL;
    uint8_t mode = SW;
    //printf ("Application running \n\r" );
    
    string filename = FILENAME;
//     if ( argc > 1 )
//     {
//     
//         filename = argv[1];
//     }
    //while ( application_run )
    //{
        if ( argc > 1 )
        {
            module = atoi ( argv[1] );
            if ( module < PREPROCESSING || module > ALL )
            {
                module = ALL;
            }
        }
        if ( argc > 2 )
        {
            mode = atoi ( argv[2] );
            if ( mode < SW || mode > HW )
            {
                mode = SW;    
            }
            
         }
        
        //wfs_get_image ( filename );
        printf ("\n\r==========================\n\r");
        printf ("Starting Computations\n\r");

        aos ( module, mode );
        usleep ( 1 ); 
    //}
}

int main( int argc, char *argv[] ) 
{  
    application_init ( );
    application ( argc, argv );   
    application_end ();
}
