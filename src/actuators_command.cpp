/*
*  C++ Implementation: preprocessing
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>

#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>

#include "sys_util.h"

#include "ao_parameters.h"
#include "actuators_command.h"


uint32_t actuators_command_sw ( )
{
    
    long long start_time = 0, end_time = 0;
    uint32_t elapsed_time = 0;

    
    start_time = getMicrotime();

        
    for (int i = 0; i < D_SIZE; i++) 
    {
        for (int j = 0; j < D_SIZE; j++) 
        {
            c_out[i] += m[i][j] * c[j];
        }
    }

    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    printf ("SW Execution Time[us]: %d \n\r", elapsed_time);
    
    return elapsed_time;

}

#include "matvect_regs.h"


uint32_t actuators_command_hw (  )
{
    //hw_accelerator ( ACTUATORS_COMMAND );
    
    
    #define ACTUATORS_COMMAND_BASE_ADDRESS ((AXI_BRIDGE_BASE) + 0x00038000)

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF0_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF0_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF1_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF1_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF2_ADDR_REG_OFST, 64);
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF2_ADDR_REG_OFST, 64 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF3_ADDR_REG_OFST, 64);
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF3_ADDR_REG_OFST, 64);


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF0_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF0_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF1_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF1_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF2_ADDR_REG_OFST, 64);
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF2_ADDR_REG_OFST, 64 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF3_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF3_ADDR_REG_OFST, 64 );



    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF0_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF0_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF1_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF1_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF2_ADDR_REG_OFST, 64);
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF2_ADDR_REG_OFST, 64 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF3_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF3_ADDR_REG_OFST, 64 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF0_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF0_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF1_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF1_ADDR_REG_OFST, 64 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF2_ADDR_REG_OFST, 64);
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF2_ADDR_REG_OFST, 64 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF3_ADDR_REG_OFST, 64 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF3_ADDR_REG_OFST, 64 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_0_REG_OFST, 0xA800A8 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_0_REG_OFST, 0xA800A8 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_1_REG_OFST, 0xA800A8 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_1_REG_OFST, 0xA800A8 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_0_REG_OFST, 0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_0_REG_OFST, 0x320032 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_0_REG_OFST,  0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_0_REG_OFST,  0x320032 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_0_REG_OFST,  0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_0_REG_OFST,  0x320032 );
    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_BUFF_DIRECTION_REG_OFST, 0 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_RD_BUFF_DIRECTION_REG_OFST, 0 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_BUFF_DIRECTION_REG_OFST, 0 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_WR_BUFF_DIRECTION_REG_OFST, 0 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_BURST_LEN_REG_OFST, 128 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_BURST_LEN_REG_OFST, 128 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_ADDR_REG_OFST, 8 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_ADDR_REG_OFST, 8 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_CRAM_LEN_REG_OFST, 0x304 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_CRAM_LEN_REG_OFST, 0x304 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_MD_ADDR_CRAM_REG_OFST, 0 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_MD_ADDR_CRAM_REG_OFST, 0 );

    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_AC_ADDR_CRAM_REG_OFST, 0x2A0 );
    read_reg_check ( ACTUATORS_COMMAND_BASE_ADDRESS+MATVECT_MAT_AC_ADDR_CRAM_REG_OFST, 0x2A0 );


    
    write_reg (ACTUATORS_COMMAND_BASE_ADDRESS+        MATVECT_CONTROL_REG_OFST, 0xB );
    //read_reg_check (ACTUATORS_COMMAND_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 168<<16 + 168 );
    
    uint32_t status_acc = 0;

    uint32_t status_acc2 = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ MATVECT_STATUS_REG_OFST );
    
    uint32_t cycles = 0;


    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

#define TIMEOUT     4
    uint32_t timeout_count = 0;
    
    do 
    {
        status_acc = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ MATVECT_STATUS_REG_OFST );
        //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
        //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
        //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );

        //if ( timeout_count++ > TIMEOUT ) break;
            
        usleep (250000);
        //printf ("waiting for ... status %x \n\r", status_acc); 


    }while ( !(status_acc & 0x01) );

    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;
    //printf ("status %x \n\r", status_acc); 

     //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
     //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
     //cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
        cycles = read_reg ( ACTUATORS_COMMAND_BASE_ADDRESS+ MATVECT_OVERALL_DURATION_REG_OFST );

     printf ("cycles %d \n\r", cycles); 

//     
     //return (cycles/1000*PERIOD);
     
    return (cycles_to_usec ( cycles ));

    
    //return elapsed_time;

    
    
}


void actuators_command_output ( void )
{
    printf( "r: ");

    for ( int indx = 0; indx < D_SIZE; indx++ )
    {
        if ( indx != 0 )
        {
            printf( ", ");
        }
        printf( "%f ", c_out[indx] );
    }
    printf( "\n\r" );

}


uint32_t actuators_command ( uint8_t mode )
{
    
    printf ("\n\rActuators Commands Stage\n\r");  

    uint32_t execution_time;
    
    if ( mode == SW )
    {
        printf ("\n\rSW Execution Model Selected \n\r");
        execution_time = actuators_command_sw();
        //actuators_command_output ();
        
    }
    else
    {
        printf ("\n\rHW Execution Model Selected \n\r");
        execution_time = actuators_command_hw();
    }

    return execution_time;    
}

