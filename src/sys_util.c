/*
*  C Implementation: swift execution controller
*
* Description: 
* This module implements the memory interface 
*
* Rev. 0.1
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

/**
 * @file sys_util.c
 * @author Andrea Guerrieri - Processor Architecture Laboratory <andrea.guerrieri@epfl.ch>
 * @date 9 Sep 2017
 * @version 0.1
 * @copyright 2017 See COPYING file that comes with this distribution
 * @brief This module implements some system utilities function.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <time.h>
#include <sched.h>  
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fstream>
#include <string>
#include <vector>
#include "sys_util.h"

#include "ao_parameters.h"


using namespace std;

#define DEBUG_SYS_UTIL_MODULE(_X) 	//_X



#include <sys/time.h>

volatile uint32_t *fpga_ddr_memory;

uint8_t application_run;

long long get_timestamp( void )
{
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
    //printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}


/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */
// cpu_num argument below should be 0 or 1 for Zynq

void assign_cpu( int cpu_num ) 
{
  cpu_set_t mask;          // for setting CPU affinity

  // Always keeping logging/monitoring process in one CPU
  //printf( "Set CPU %d \n\r", cpu_num );
  CPU_ZERO(&mask);
  CPU_SET(cpu_num, &mask);
  sched_setaffinity(0, sizeof(mask), &mask);       // setting current process  
}


/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */
void core_dump_init ( void )
{
    uint32_t ret;
    struct rlimit rlim;

    /* Get the Dumpable state */
    ret = prctl( PR_GET_DUMPABLE, 0, 0, 0, 0 );
    
    /* Set the Dumpable state to be enabled */
    ret = prctl( PR_SET_DUMPABLE, 1, 0, 0, 0 );
    
    /* Get the Dumpable state again, make sure it was set */
    ret = prctl( PR_GET_DUMPABLE, 0, 0, 0, 0 );

    ret = getrlimit ( RLIMIT_CORE, &rlim );

    /* Set the core dump limitation to be unlimited */
    rlim.rlim_cur = RLIM_INFINITY;
    rlim.rlim_max = RLIM_INFINITY;
    ret = setrlimit ( RLIMIT_CORE, &rlim );

    /* Get the core dump limitation again */
    ret = getrlimit ( RLIMIT_CORE, &rlim );

    FILE * fp;
    fp = fopen ( FILE_PID, "w+" );
    fprintf(fp, "%d", (int) getpid( ) );
    fclose(fp);

}

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */
void arguments_parser ( uint32_t argc,  char *argv[] )
{
    return;
}

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */
void signal_handler ( void )
{
	struct sigaction sig_term;

	sig_term.sa_handler = termination_handler;
	sig_term.sa_flags = SA_RESTART;
	sig_term.sa_restorer = NULL;
	sigaction ( SIGTERM, &sig_term, NULL );
	sigaction ( SIGQUIT, &sig_term, NULL );
	sigaction ( SIGINT, &sig_term, NULL );
}

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */
void termination_handler ( uint32_t sig )
{
	switch ( sig )
	{
		case SIGTERM:
		case SIGQUIT:
		case SIGINT:
			remove (FILE_PID);
            application_run = FALSE;
			break;
	}
	
	
}

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */

void system_timer ( uint32_t timebase )
{
     usleep ( timebase ); //1 [ms]
}

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 * @note Something to note.
 * @warning Warning.
 */

uint32_t fpga_ddr_memory_init (uint32_t address, uint32_t size )
{
#if 1
    uint32_t fd;
    uint32_t indx;
    
    fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd < 0) {
      fprintf(stderr, "open(/dev/mem) failed (%d)\n", errno);
      return 0;
    }

    fpga_ddr_memory = mmap (NULL, size , PROT_READ | PROT_WRITE, MAP_SHARED, fd, address );
    if ( fpga_ddr_memory == MAP_FAILED ) 
    {
      fprintf(stderr, "mmap64(0x%x@0x%x) failed (%d)\n", size, (uint32_t)( address ), errno);
      return 0;
    }
    else
    {
      printf( "mmap success\n\r" );
    }

#if 0
    printf( "cleaning-up ddr_fpga memory..." );
    fflush( stdout );
    for ( indx = 0; indx < (size/4); indx++ )
//    for ( indx = 0; indx < 4; indx++ )
    {
        fpga_ddr_memory[indx] = 0xDEADBEEF;
      if ( indx % (1*(1024*1024)) == 0 )
      {
          printf(".");
          fflush(stdout);
      }
    }
    printf( "done\n\r" );
#endif


#if 0
    printf( "cleaning-up ddr_fpga memory..." );
    fflush( stdout );
    int value;
    value = 1;
    for ( indx = 0; indx < 1024*1024; indx++ )
    {
        
        fpga_ddr_memory[indx] = ((value+1)<<16) + value;
      if ( indx % (1*(1024*1024)) == 0 )
      {
          printf(".");
          fflush(stdout);
      }
      value += 2;
    }
    printf( "done\n\r" );
    exit (1);
#endif

#endif
return 0;
}


uint32_t pl_write_file ( char *filename )
{
    
    FILE *fp;
    int val, indx;
    
    fp = fopen( filename ,"r");
    
    indx = 0;
    
    if ( fp > 0 )
    {
      while( indx <= 20093400 )
      {
	  fscanf(fp, "%X ", &val);
	  //printf ("%X\n\r", val );
	           fpga_ddr_memory[indx] = val;
	  indx++;
	  //sleep (1);
      }
    }
    
}

uint32_t pl_read_file ( char *filename )
{
    FILE *fp;
    int val, indx;
    
    fp = fopen( filename ,"r");
    
    indx = 0;
    
    if ( fp > 0 )
    {
      while( indx <= 20093400 )
      {
	  //printf ("%X\n\r", val );
	  val = fpga_ddr_memory[indx];
      fprintf(fp, "%X ", val );
	  indx++;
	  //sleep (1);
      }
    }
    
}



#define PAGE_SIZE ((size_t)getpagesize())
#define PAGE_MASK ((uint64_t)(long)~(PAGE_SIZE - 1))

uint32_t write_reg ( size_t address, uint32_t value )
{
    printf("DEBUG: Writing reg at address 0x%x\n", address);

#if 1

    int fd;
    volatile void *acc_p;
	    
	fd = open("/dev/mem", O_RDWR | O_DSYNC);

	if (fd < 0) {
	  fprintf(stderr, "open(/dev/mem) failed (%d)\n", errno);
	  return -1;
	}

	// acc_p = mmap ( NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, aligned_addr );
	acc_p = mmap ( 0, 2* PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, address & ~ (typeof(address))(PAGE_SIZE-1) );
    if (acc_p == (void*) -1) {
        printf("mmap failed.\n");
        exit(1);
    }

    size_t offset = (unsigned int)(address & (PAGE_SIZE-1));

	volatile uint32_t* aligned_addr =  acc_p + offset;//+  ( address & PAGE_MASK);
	
    printf("DEBUG: BEFORE aligned acces\n");
	// *(volatile uint32_t *)( acc_p +  address - aligned_addr )  = value ;

    printf("DEBUG: %x\n", *aligned_addr );
	*aligned_addr   = value ;
    printf("DEBUG: AFTER aligned access\n");
    printf("DEBUG: %x\n", *aligned_addr );

	close ( fd );
    return 0;

#endif
}

uint32_t read_reg ( uint32_t address )
{
#if 1
    uint32_t fd;
    volatile void *reg_p;
    size_t aligned_addr;
    
    aligned_addr = address & PAGE_MASK;
    
    fd = open("/dev/mem", O_RDWR | O_DSYNC);
    if (fd < 0) {
      fprintf(stderr, "open(/dev/mem) failed (%d)\n", errno);
      return -1;
    }

    reg_p = mmap (NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, aligned_addr );
    if ( reg_p == MAP_FAILED ) 
    {
      //fprintf(stderr, "mmap64(0x%x@0x%x) failed (%d)\n", PAGE_SIZE, (uint32_t)( aligned_addr ), errno);
      return -1;
    }

    uint32_t result = ( *(volatile uint32_t *)(  reg_p +  address - aligned_addr ) );
    
    close ( fd );

    return result;
#endif
}


void read_reg_check ( uint32_t address,  uint32_t value )
{
#if 1
      uint32_t read_val;
      read_val =  read_reg ( address );
      if ( read_val == value )
      {
	printf ("addres %x, value %x : CORRECT\n\r", address, read_val );  
      }
      else
      {
	printf ("addres %x, value %x , expected %x : ERROR \n\r", address, read_val, value );  
      }
#endif
}



#include <sys/time.h>

long long current_timestamp( void )
{
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
    //printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}


long long getMicrotime( void )
{
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	//printf("microseconds: %lld\n", currentTime.tv_sec * (int)1e6 + currentTime.tv_usec);
    return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
    
}

uint32_t cycles_to_usec ( uint32_t cycles )
{
    float usec;
    
    usec = ((float) cycles / 1000 ) * PERIOD;
    
    //printf ( "usec %f:\n\r", usec );
    
    return ((uint32_t) usec);
}

#include "preprocessing_regs.h"

void hw_accelerators_init( void )
{

    printf("DEBUG: DEISOLATE_VALUE: 0x%x\n", DEISOLATE_VALUE);

    write_reg (AAW0C_BASE + ( 0x10000 * 1 ), DEISOLATE_VALUE );


    //     printf ("Bypass TLB\n\r");  
    write_reg ((AXI_BRIDGE_BASE) + 0x000200E0, 0x20 );
// 
    //read_reg_check ( 0xFF2200E4, 0x100 );
    
    write_reg (AAW0C_BASE + ( 0x10000 * 1 ), RESET_PR );

    //usleep (250000);


}

uint32_t hw_accelerator ( uint8_t region )
{
    
    //printf ("\n\rTest_application\n\r");  
    //printf ("\n\rPreprocessing Stage\n\r");  
    //printf ("\n\rModal Decomposition Stage\n\r");  
    //printf ("\n\rOptimal Controls Stage\n\r");  
    //printf ("\n\rActuators Commands Stage\n\r");  
    
//     uint32_t burst_len = 64;
//     
//     long long start_time, end_time;
//     uint32_t elapsed_time;
// 
//     start_time = current_timestamp();

//     printf ("Bypass TLB\n\r");  
    write_reg ((AXI_BRIDGE_BASE) + 0x000200E0, 0x20 );
// 
    read_reg_check ((AXI_BRIDGE_BASE) + 0x000200E4, 0x100 );
    
    write_reg (AAW0C_BASE + ( 0x10000 * 1 ), RESET_PR );

    //usleep (250000);

    write_reg (AAW0C_BASE + ( 0x10000 * 1 ), DEISOLATE_VALUE );
    

#define PREPROCESSING_BASE_ADDRESS ((AXI_BRIDGE_BASE) + 0x00028000)
    //printf ("src PL Memory address\n\r");  
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, 1 );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, 1 );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFX_LEN_REG_OFST, 168 );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_RD_BUFFX_LEN_REG_OFST, 168 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x8 );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x8 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x8 );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x8 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );

    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, ((168<<16) + 168) );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, ((168<<16) + 168) );

    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, ((168<<16) + 168) );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, ((168<<16) + 168) );


    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 168<<16 + 168 );
    
    uint32_t status_acc = 0;

    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

    do 
    {
        status_acc = read_reg ( PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_STATUS_REG_OFST );
        //usleep (250000);
        //printf ("waiting for ... status %x \n\r", status_acc); 


    }while ( status_acc & 0x01 );

//     uint32_t cycles = 0;
//     cycles = read_reg ( BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
//     
//     return (cycles*10000);
       
    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;
    
    return elapsed_time;


#if 0
    //printf ("len PL Memory transfer\n\r"); 

    write_reg (0xFF218004, LENGTH );
    read_reg_check ( 0xFF218004, LENGTH );


    //printf ("burst PL Memory transfer\n\r"); 

    write_reg (0xFF218008, burst_len );
    read_reg_check ( 0xFF218008, burst_len );




    write_reg (0xFF200000, 0x0 ); //Disable AIC Interrupt Forward
    
    //usleep (250000);

    write_reg (0xFF210000, 0x40 ); //Enable AAWC Interrupt Forward


    //usleep (250000);

    end_time = current_timestamp();
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    //printf ("Setup time: [ns] %d \n\r", elapsed_time);

    start_time = current_timestamp();

    write_reg (0xFF21800C, 0x02 );
    //usleep (250000);
    write_reg (0xFF21800C, 0x00 );


    //printf ("Command register [ START ]\n\r"); 
    write_reg (0xFF21800C, 0x01 );


    uint32_t status_acc = 0;

    do 
    {
        status_acc = read_reg ( 0xFF218010 );
       usleep (250000);
       printf ("waiting for ... status %x \n\r", status_acc); 


    }while ( status_acc & 0x01 );


    //for ( int indx=0; indx < 5; indx++) 
    {
        int write_clk_cycles = read_reg ( 0xFF218014 );
        int read_clk_cycles = read_reg ( 0xFF218018 );



        end_time = current_timestamp();
        elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

        //printf ("write_clk_cycles: %d \n\r", write_clk_cycles);
        //printf ("read_clk_cyclese:  %d \n\r", read_clk_cycles);
        
        uint32_t execution_time = write_clk_cycles*10 + read_clk_cycles*10;
//        execution_time /= 1000;
        execution_time /= 10000000;
        //printf ("elapsed_time [ns] %d \n\r", execution_time );
        //printf ("elapsed_time [ns] %d \n\r", execution_time );
        
//         printf ("write_clk_cycles: [ns] %d \n\r", write_clk_cycles * 5 );
//         printf ("read_clk_cyclese: [ns] %d \n\r", read_clk_cycles * 5 );
        
        return execution_time;

    }
    
    //exit (0);
    
#endif

}

#if 0
uint32_t hw_accelerator ( uint8_t region )
{
    
    //printf ("\n\rTest_application\n\r");  
    //printf ("\n\rPreprocessing Stage\n\r");  
    //printf ("\n\rModal Decomposition Stage\n\r");  
    //printf ("\n\rOptimal Controls Stage\n\r");  
    //printf ("\n\rActuators Commands Stage\n\r");  
    
    uint32_t burst_len = 64;
    
    long long start_time, end_time;
    uint32_t elapsed_time;

    start_time = current_timestamp();

//     printf ("Bypass TLB\n\r");  
    write_reg (0xFF2100E0, 0x20 );
// 
    read_reg_check ( 0xFF2100E4, 0x100 );
    
    write_reg (AAW0C_BASE + ( 0x10000 * region ), RESET_PR );

    //usleep (250000);

    write_reg (AAW0C_BASE + ( 0x10000 * region ), DEISOLATE_VALUE );
    

    //printf ("src PL Memory address\n\r");  
    write_reg (0xFF218000, SRC_ADDRESS );
    read_reg_check ( 0xFF218000, SRC_ADDRESS );

    //printf ("len PL Memory transfer\n\r"); 

    write_reg (0xFF218004, LENGTH );
    read_reg_check ( 0xFF218004, LENGTH );


    //printf ("burst PL Memory transfer\n\r"); 

    write_reg (0xFF218008, burst_len );
    read_reg_check ( 0xFF218008, burst_len );




    write_reg (0xFF200000, 0x0 ); //Disable AIC Interrupt Forward
    
    //usleep (250000);

    write_reg (0xFF210000, 0x40 ); //Enable AAWC Interrupt Forward


    //usleep (250000);

    end_time = current_timestamp();
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    //printf ("Setup time: [ns] %d \n\r", elapsed_time);

    start_time = current_timestamp();

    write_reg (0xFF21800C, 0x02 );
    //usleep (250000);
    write_reg (0xFF21800C, 0x00 );


    //printf ("Command register [ START ]\n\r"); 
    write_reg (0xFF21800C, 0x01 );


    uint32_t status_acc = 0;

    do 
    {
        status_acc = read_reg ( 0xFF218010 );
       usleep (250000);
       printf ("waiting for ... status %x \n\r", status_acc); 


    }while ( status_acc & 0x01 );


    //for ( int indx=0; indx < 5; indx++) 
    {
        int write_clk_cycles = read_reg ( 0xFF218014 );
        int read_clk_cycles = read_reg ( 0xFF218018 );



        end_time = current_timestamp();
        elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

        //printf ("write_clk_cycles: %d \n\r", write_clk_cycles);
        //printf ("read_clk_cyclese:  %d \n\r", read_clk_cycles);
        
        uint32_t execution_time = write_clk_cycles*10 + read_clk_cycles*10;
//        execution_time /= 1000;
        execution_time /= 10000000;
        //printf ("elapsed_time [ns] %d \n\r", execution_time );
        //printf ("elapsed_time [ns] %d \n\r", execution_time );
        
//         printf ("write_clk_cycles: [ns] %d \n\r", write_clk_cycles * 5 );
//         printf ("read_clk_cyclese: [ns] %d \n\r", read_clk_cycles * 5 );
        
        return execution_time;

    }
    
    //exit (0);
    

}

#endif


void split(const string& s, char c, vector<string>& v) {
   string::size_type i = 0;
   string::size_type j = s.find(c);

   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);

      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

