// #############################################################################
// accelerator_throughput_benchmark_regs.h
// =======================================
// Contains the register map of the throughput benchmarking accelerator.
//
// Author : Sahand Kashani-Akhavan [sahand.kashani-akhavan@epfl.ch]
// Revision : 0.3
// Last updated : 2017-09-22 17:18:52 UTC
// #############################################################################

#ifndef __MATVECT_REGS_H__
#define __MATVECT_REGS_H__

//#include "helpers.h"

// Register map
// TODO: CHANGE REGISTER MAP DESCRIPTION !!
// ------------


#define MATVECT_CONTROL_REG_OFST				(0)		/* RW */
#define MATVECT_STATUS_REG_OFST				(4)		/* RW */
//reserved
#define MATVECT_RD_MD_BUFF0_ADDR_REG_OFST		(12)	/* RW */
#define MATVECT_RD_MD_BUFF1_ADDR_REG_OFST		(16)	/* RW */
#define MATVECT_RD_MD_BUFF2_ADDR_REG_OFST		(20)	/* RW */
#define MATVECT_RD_MD_BUFF3_ADDR_REG_OFST		(24)	/* RW */
#define MATVECT_RD_AC_BUFF0_ADDR_REG_OFST		(28)	/* RW */
#define MATVECT_RD_AC_BUFF1_ADDR_REG_OFST		(32)	/* RW */
#define MATVECT_RD_AC_BUFF2_ADDR_REG_OFST		(36)	/* RW */
#define MATVECT_RD_AC_BUFF3_ADDR_REG_OFST		(40)	/* RW */
#define MATVECT_WR_MD_BUFF0_ADDR_REG_OFST		(44)	/* RW */
#define MATVECT_WR_MD_BUFF1_ADDR_REG_OFST		(48)	/* RW */
#define MATVECT_WR_MD_BUFF2_ADDR_REG_OFST		(52)	/* RW */
#define MATVECT_WR_MD_BUFF3_ADDR_REG_OFST		(56)	/* RW */
#define MATVECT_WR_AC_BUFF0_ADDR_REG_OFST		(60)	/* RW */
#define MATVECT_WR_AC_BUFF1_ADDR_REG_OFST		(64)	/* RW */
#define MATVECT_WR_AC_BUFF2_ADDR_REG_OFST		(68)	/* RW */
#define MATVECT_WR_AC_BUFF3_ADDR_REG_OFST		(72)	/* RW */
#define MATVECT_RD_MD_BUFF_LEN_0_REG_OFST		(76)	/* RW */
#define MATVECT_RD_MD_BUFF_LEN_1_REG_OFST		(80)	/* RW */
#define MATVECT_RD_AC_BUFF_LEN_0_REG_OFST		(84)	/* RW */
#define MATVECT_RD_AC_BUFF_LEN_1_REG_OFST		(88)	/* RW */
#define MATVECT_WR_MD_BUFF_LEN_0_REG_OFST		(92)	/* RW */
#define MATVECT_WR_MD_BUFF_LEN_1_REG_OFST		(96)	/* RW */
#define MATVECT_WR_AC_BUFF_LEN_0_REG_OFST		(100)	/* RW */
#define MATVECT_WR_AC_BUFF_LEN_1_REG_OFST		(104)	/* RW */
#define MATVECT_RD_BUFF_DIRECTION_REG_OFST		(108)	/* RW */
#define MATVECT_WR_BUFF_DIRECTION_REG_OFST		(112)	/* RW */
#define MATVECT_BURST_LEN_REG_OFST				(116)	/* RW */
#define MATVECT_MAT_ADDR_REG_OFST				(120)	/* RW */
#define MATVECT_MAT_CRAM_LEN_REG_OFST			(124)	/* RW */
#define MATVECT_MAT_MD_ADDR_CRAM_REG_OFST		(128)	/* RW */
#define MATVECT_MAT_AC_ADDR_CRAM_REG_OFST		(132)	/* RW */
#define MATVECT_READ_DURATION_REG_OFST			(136)	/* RW */
#define MATVECT_WRITE_DURATION_REG_OFST			(140)	/* RW */
#define MATVECT_OVERALL_DURATION_REG_OFST		(144)	/* RW */
//RESERVED
#define MATVECT_RUN_TIMER_REG_OFST				(152)	/* RW */





// CONTROL register (various fields)
// =================================
// Bit widths.
#define MATVECT_CONTROL_START_WIDTH	(1)
#define MATVECT_CONTROL_MODE_WIDTH	(3)
#define MATVECT_CONTROL_STOP_WIDTH	(1)
// Bit indices.
#define MATVECT_CONTROL_START_IDX		(0)	// active high
#define MATVECT_CONTROL_MODE_IDX		(1) // 0:Pipeline mode;  1:Asynchronous
#define MATVECT_CONTROL_STOP_IDX		(4) // active high
// Bit masks.
#define MATVECT_CONTROL_START_MASK		(CREATE_MASK_SHIFTED(MATVECT_CONTROL_START_WIDTH, MATVECT_CONTROL_START_IDX))
#define MATVECT_CONTROL_MODE_MASK		(CREATE_MASK_SHIFTED(MATVECT_CONTROL_MODE_WIDTH, MATVECT_CONTROL_MODE_IDX))
#define MATVECT_CONTROL_STOP_MASK		(CREATE_MASK_SHIFTED(MATVECT_CONTROL_STOP_WIDTH, MATVECT_CONTROL_STOP_IDX))


// STATUS register (various fields)
// ================================
// Bit widths.
#define MATVECT_STATUS_DONE_WIDTH		(1)
#define MATVECT_STATUS_RUNNING_WIDTH	(1)
// Bit indices.
#define MATVECT_STATUS_DONE_IDX		(0)	// active high
#define MATVECT_STATUS_RUNNING_IDX	(2) // active high
// Bit masks.
#define MATVECT_STATUS_DONE_MASK		(CREATE_MASK_SHIFTED(MATVECT_STATUS_DONE_WIDTH, MATVECT_STATUS_DONE_IDX))
#define MATVECT_STATUS_RUNNING_MASK		(CREATE_MASK_SHIFTED(MATVECT_STATUS_RUNNING_WIDTH, MATVECT_STATUS_RUNNING_IDX))



// XX_BUFF_LEN_X registers (various fields)
// ================================
// Bit widths.
#define MATVECT_XX_BUFF_LEN_X_WIDTH	(16)
// Bit indices.
#define MATVECT_XX_BUFF_LEN_0_IDX	(0)	// unsigned value: number of words in the buffer
#define MATVECT_XX_BUFF_LEN_1_IDX	(16)	// unsigned value: number of words in the buffer
// Bit masks.
#define MATVECT_XX_BUFF_LEN_0_MASK		(CREATE_MASK_SHIFTED(MATVECT_XX_BUFF_LEN_X_WIDTH, MATVECT_XX_BUFF_LEN_0_IDX))
#define MATVECT_XX_BUFF_LEN_1_MASK		(CREATE_MASK_SHIFTED(MATVECT_XX_BUFF_LEN_X_WIDTH, MATVECT_XX_BUFF_LEN_1_IDX))


// RD_BUFF_DIRECTION register (various fields)
// ================================
// Bit widths.
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF0_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF1_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF2_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF3_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF0_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF1_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF2_WIDTH	(1)
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF3_WIDTH	(1)
// Bit indices.
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF0_IDX		(0)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF1_IDX		(1)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF2_IDX		(2)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_MD_BUFF3_IDX		(3)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF0_IDX		(4)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF1_IDX		(5)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF2_IDX		(6)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_RD_BUFF_DIRECTION_AC_BUFF3_IDX		(7)	//0: Addresses increased;   1: Addresses decreased


// WR_BUFF_DIRECTION register (various fields)
// ================================
// Bit widths.
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF0_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF1_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF2_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF3_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF0_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF1_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF2_WIDTH	(1)
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF3_WIDTH	(1)
// Bit indices.
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF0_IDX		(0)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF1_IDX		(1)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF2_IDX		(2)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_MD_BUFF3_IDX		(3)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF0_IDX		(4)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF1_IDX		(5)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF2_IDX		(6)	//0: Addresses increased;   1: Addresses decreased
#define MATVECT_WR_BUFF_DIRECTION_AC_BUFF3_IDX		(7)	//0: Addresses increased;   1: Addresses decreased


// BURST_LEN register (various fields)
// ================================
// Bit widths.
#define MATVECT_BURST_LEN_WIDTH	(8)
// Bit indices.
#define MATVECT_BURST_LEN_IDX	(0)	// unsigned value: number of words in a line
// Bit masks.
#define MATVECT_BURST_LEN_MASK	(CREATE_MASK_SHIFTED(MATVECT_BURST_LEN_WIDTH, MATVECT_BURST_LEN_IDX))


// MAT_CRAM_LEN register (various fields)
// ================================
// Bit widths.
#define MATVECT_MAT_CRAM_LEN_WIDTH	(16)
// Bit indices.
#define MATVECT_MAT_CRAM_LEN_IDX	(0)	// unsigned value: number of words in a line
// Bit masks.
#define MATVECT_MAT_CRAM_LEN_MASK	(CREATE_MASK_SHIFTED(MATVECT_MAT_CRAM_LEN_WIDTH, MATVECT_MAT_CRAM_LEN_IDX))



#endif /* __MATVECT_REGS_H__ */
