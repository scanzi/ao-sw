// #############################################################################
// accelerator_throughput_benchmark_regs.h
// =======================================
// Contains the register map of the throughput benchmarking accelerator.
//
// Author : Sahand Kashani-Akhavan [sahand.kashani-akhavan@epfl.ch]
// Revision : 0.3
// Last updated : 2017-09-22 17:18:52 UTC
// #############################################################################

#ifndef __IIR_REGS_H__
#define __IIR_REGS_H__

//#include "helpers.h"

// Register map
// TODO: CHANGE REGISTER MAP DESCRIPTION !!
// ------------


#define IIR_CONTROL_REG_OFST				(0)		/* RW */
#define IIR_STATUS_REG_OFST					(4)		/* RW */
//reserved
#define IIR_RD_BUFF0_ADDR_REG_OFST			(12)	/* RW */
#define IIR_RD_BUFF1_ADDR_REG_OFST			(16)	/* RW */
#define IIR_RD_BUFF2_ADDR_REG_OFST			(20)	/* RW */
#define IIR_RD_BUFF3_ADDR_REG_OFST			(24)	/* RW */

#define IIR_RD_BUFF_LEN_0_REG_OFST			(28)	/* RW */
#define IIR_RD_BUFF_LEN_1_REG_OFST			(32)	/* RW */
#define IIR_RD_BUFF_DIR_REG_OFST			(36)	/* RW */
#define IIR_RD_BURST_LEN_REG_OFST			(40)	/* RW */

#define IIR_WR_BUFF0_ADDR_REG_OFST			(44)	/* RW */
#define IIR_WR_BUFF1_ADDR_REG_OFST			(48)	/* RW */
#define IIR_WR_BUFF2_ADDR_REG_OFST			(52)	/* RW */
#define IIR_WR_BUFF3_ADDR_REG_OFST			(56)	/* RW */

#define IIR_WR_BUFF_LEN_0_REG_OFST			(60)	/* RW */
#define IIR_WR_BUFF_LEN_1_REG_OFST			(64)	/* RW */
#define IIR_WR_BUFF_DIR_REG_OFST			(68)	/* RW */

#define IIR_READ_DURATION_REG_OFST			(72)	/* RW */
#define IIR_WRITE_DURATION_REG_OFST			(76)	/* RW */
#define IIR_OVERALL_DURATION_REG_OFST		(80)	/* RW */
//RESERVED
#define IIR_RUN_TIMER_REG_OFST				(88)	/* RW */

#define IIR_A_FACTORS_REGS_OFST				(92)	/* RW */
#define IIR_B_FACTORS_REGS_OFST				(118)	/* RW */
#define IIR_MUL_FACTORS_REGS_OFST			(213)	/* RW */



// CONTROL register (various fields)
// =================================
// Bit widths.
#define IIR_CONTROL_START_WIDTH		(1)
#define IIR_CONTROL_MODE_WIDTH		(1)
#define IIR_CONTROL_STOP_WIDTH		(1)
// Bit indices.
#define IIR_CONTROL_START_IDX		(0)	// active high
#define IIR_CONTROL_MODE_IDX		(1) // 0:Pipeline mode;  1:Asynchronous
#define IIR_CONTROL_STOP_IDX		(2) // active high
// Bit masks.
#define IIR_CONTROL_START_MASK		(CREATE_MASK_SHIFTED(IIR_CONTROL_START_WIDTH, IIR_CONTROL_START_IDX))
#define IIR_CONTROL_MODE_MASK		(CREATE_MASK_SHIFTED(IIR_CONTROL_MODE_WIDTH, IIR_CONTROL_MODE_IDX))
#define IIR_CONTROL_STOP_MASK		(CREATE_MASK_SHIFTED(IIR_CONTROL_STOP_WIDTH, IIR_CONTROL_STOP_IDX))


// STATUS register (various fields)
// ================================
// Bit widths.
#define IIR_STATUS_DONE_WIDTH		(1)
#define IIR_STATUS_RUNNING_WIDTH	(1)
// Bit indices.
#define IIR_STATUS_DONE_IDX			(0)	// active high
#define IIR_STATUS_RUNNING_IDX		(2) // active high
// Bit masks.
#define IIR_STATUS_DONE_MASK		(CREATE_MASK_SHIFTED(IIR_STATUS_DONE_WIDTH, IIR_STATUS_DONE_IDX))
#define IIR_STATUS_RUNNING_MASK		(CREATE_MASK_SHIFTED(IIR_STATUS_RUNNING_WIDTH, IIR_STATUS_RUNNING_IDX))



// XX_BUFF_LEN_X registers (various fields)
// ================================
// Bit widths.
#define IIR_XX_BUFF_LEN_X_WIDTH		(16)
// Bit indices.
#define IIR_XX_BUFF_LEN_0_IDX		(0)	// unsigned value: number of words in the buffer
#define IIR_XX_BUFF_LEN_1_IDX		(16)	// unsigned value: number of words in the buffer
// Bit masks.
#define IIR_XX_BUFF_LEN_0_MASK		(CREATE_MASK_SHIFTED(IIR_XX_BUFF_LEN_X_WIDTH, IIR_XX_BUFF_LEN_0_IDX))
#define IIR_XX_BUFF_LEN_1_MASK		(CREATE_MASK_SHIFTED(IIR_XX_BUFF_LEN_X_WIDTH, IIR_XX_BUFF_LEN_1_IDX))


// XX_BUFF_DIRECTION register (various fields)
// ================================
// Bit widths.
#define IIR_RD_BUFF_DIRECTION_BUFF0_WIDTH	(1)
#define IIR_RD_BUFF_DIRECTION_BUFF1_WIDTH	(1)
#define IIR_RD_BUFF_DIRECTION_BUFF2_WIDTH	(1)
#define IIR_RD_BUFF_DIRECTION_BUFF3_WIDTH	(1)
// Bit indices.
#define IIR_RD_BUFF_DIRECTION_BUFF0_IDX		(0)	//0: Addresses increased;   1: Addresses decreased
#define IIR_RD_BUFF_DIRECTION_BUFF1_IDX		(1)	//0: Addresses increased;   1: Addresses decreased
#define IIR_RD_BUFF_DIRECTION_BUFF2_IDX		(2)	//0: Addresses increased;   1: Addresses decreased
#define IIR_RD_BUFF_DIRECTION_BUFF3_IDX		(3)	//0: Addresses increased;   1: Addresses decreased


// WR_BUFF_DIRECTION register (various fields)
// ================================
// Bit widths.
#define IIR_WR_BUFF_DIRECTION_BUFF0_WIDTH		(1)
#define IIR_WR_BUFF_DIRECTION_BUFF1_WIDTH		(1)
#define IIR_WR_BUFF_DIRECTION_BUFF2_WIDTH		(1)
#define IIR_WR_BUFF_DIRECTION_BUFF3_WIDTH		(1)
// Bit indices.
#define IIR_WR_BUFF_DIRECTION_BUFF0_IDX			(0)	//0: Addresses increased;   1: Addresses decreased
#define IIR_WR_BUFF_DIRECTION_BUFF1_IDX			(1)	//0: Addresses increased;   1: Addresses decreased
#define IIR_WR_BUFF_DIRECTION_BUFF2_IDX			(2)	//0: Addresses increased;   1: Addresses decreased
#define IIR_WR_BUFF_DIRECTION_BUFF3_IDX			(3)	//0: Addresses increased;   1: Addresses decreased


// BURST_LEN register (various fields)
// ================================
// Bit widths.
#define IIR_BURST_LEN_WIDTH	(8)
// Bit indices.
#define IIR_BURST_LEN_IDX	(0)	// unsigned value: number of words in a line
// Bit masks.
#define IIR_BURST_LEN_MASK	(CREATE_MASK_SHIFTED(IIR_BURST_LEN_WIDTH, IIR_BURST_LEN_IDX))


// IIR_A_FACTORS_REGS_OFST register (various fields)
// ================================
// Organization:
// IIR0_A0
// ...
// IIR0_A18
// IIR1_A0
// ...
// IIR1_A18
// ...
// ...
// IIR4_A18
#define IIR_N_A_FACTORS_REGS		(95)


// IIR_B_FACTORS_REGS_OFST register (various fields)
// WARNING: different sign convention: y(k+1)= Ax + By
// ================================
// Organization:
// IIR0_B0
// ...
// IIR0_B18
// IIR1_B0
// ...
// IIR1_B18
// ...
// ...
// IIR4_B18
#define IIR_N_B_FACTORS_REGS		(95)


// IIR_MUL_FACTORS_REGS_OFST register (various fields)
// ================================
#define IIR_N_MUL_FACTORS_REGS		(395)



#endif /* __MATVECT_REGS_H__ */
