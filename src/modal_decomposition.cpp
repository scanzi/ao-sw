/*
*  C++ Implementation: preprocessing
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>
#include <fstream>


#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>

#include "sys_util.h"


#include "ao_parameters.h"
#include "modal_decomposition.h"



#define FILENAME "d.h"

ofstream d_matrix_file;

void generate_d_matrix ( void )
{
    
    
    d_matrix_file.open (FILENAME);
    
    for (int i = 0; i < N_SIZE; i++) 
    {
        if ( i != 0 )
        {
            d_matrix_file <<  ","; 
        }
        
        d_matrix_file << "{";
        //printf ("{ ");
        

        for (int j = 0; j < D_SIZE; j++) 
        {   
            if ( j != 0 )
            {
                d_matrix_file <<  ","; 
            }
            //printf ( "%2f ", float(i) );
            d_matrix_file << float(i);

            
            d [j][i] = float(i);
            
        }
        //printf (" }\n\r");
        d_matrix_file << "}";

    }
    
    d_matrix_file.close();

}



uint32_t modal_decomposition_sw ( )
{
    long long start_time, end_time;
    uint32_t elapsed_time;

    //generate_d_matrix ( );
    
    start_time = getMicrotime();

    for (int i = 0; i < D_SIZE; i++) 
    {
        for (int j = 0; j < N_SIZE; j++) 
        {
            r[i][0] += d[j][i] * slopes[j];  
        }
    }

    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    printf ("SW Execution Time [us]: %d \n\r", elapsed_time);

    return elapsed_time;
    
}

#include "matvect_regs.h"

uint32_t modal_decomposition_hw (  )
{
    //hw_accelerator ( MODAL_DECOMPOSITION );
    
   
    #define MODAL_DECOMPOSITION_BASE_ADDRESS ((AXI_BRIDGE_BASE) + 0x00038000)

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS);


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF2_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );



    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF2_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_0_REG_OFST, 0xA800A8 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_0_REG_OFST, 0xA800A8 );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_1_REG_OFST, 0xA800A8 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_MD_BUFF_LEN_1_REG_OFST, 0xA800A8 );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_0_REG_OFST, 0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_0_REG_OFST, 0x320032 );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_AC_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_0_REG_OFST,  0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_0_REG_OFST,  0x320032 );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_MD_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_0_REG_OFST,  0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_0_REG_OFST,  0x320032 );
    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_1_REG_OFST,  0x320032 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_AC_BUFF_LEN_1_REG_OFST,  0x320032 );


    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_BUFF_DIRECTION_REG_OFST, 0 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_RD_BUFF_DIRECTION_REG_OFST, 0 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_BUFF_DIRECTION_REG_OFST, 0 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_WR_BUFF_DIRECTION_REG_OFST, 0 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_BURST_LEN_REG_OFST, 192 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_BURST_LEN_REG_OFST, 192 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_ADDR_REG_OFST, 8 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_ADDR_REG_OFST, 8 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_CRAM_LEN_REG_OFST, 0x304 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_CRAM_LEN_REG_OFST, 0x304 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_MD_ADDR_CRAM_REG_OFST, 0 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_MD_ADDR_CRAM_REG_OFST, 0 );

    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_AC_ADDR_CRAM_REG_OFST, 0x2A0 );
    read_reg_check ( MODAL_DECOMPOSITION_BASE_ADDRESS+MATVECT_MAT_AC_ADDR_CRAM_REG_OFST, 0x2A0 );


    
    write_reg (MODAL_DECOMPOSITION_BASE_ADDRESS+        MATVECT_CONTROL_REG_OFST, 0x9 );
    //read_reg_check (MODAL_DECOMPOSITION_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 168<<16 + 168 );
    
    uint32_t status_acc = 0;

    uint32_t status_acc2 = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ MATVECT_STATUS_REG_OFST );
    
    uint32_t cycles = 0;


    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

#define TIMEOUT     4
    uint32_t timeout_count = 0;
    
    do 
    {
        status_acc = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ MATVECT_STATUS_REG_OFST );
        //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
        //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
        //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
        //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ MATVECT_RUN_TIMER_REG_OFST );

        //if ( timeout_count++ > TIMEOUT ) break;
            
        usleep (250000);
        //printf ("waiting for ... status %x \n\r", status_acc); 
        //printf ("runtime register %x \n\r", cycles); 


    }while ( !(status_acc & 0x01) );

    cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ MATVECT_OVERALL_DURATION_REG_OFST );

    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;
    //printf ("status %x \n\r", status_acc); 

     //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
     //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
     //cycles = read_reg ( MODAL_DECOMPOSITION_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
     printf ("cycles %d \n\r", cycles); 

     //return (cycles/1000*PERIOD);
     
     return (cycles_to_usec ( cycles ));
       
    
    //return elapsed_time;

}


void modal_decomposition_output ( void )
{
    printf( "r: ");

    for ( int indx = 0; indx < D_SIZE; indx++ )
    {
        if ( indx != 0 )
        {
            printf( ", ");
        }
#ifdef USE_FLOAT
        printf( "%f ", r[indx][0] );
#else
        printf( "%d ", r[indx][0] );        
#endif
    }
    printf( "\n\r" );

}

uint32_t modal_decomposition ( uint8_t mode )
{
    
    printf ("\n\rModal Decomposition Stage\n\r");  

    uint32_t execution_time;
    
    if ( mode == SW )
    {
        printf ("\n\rSW Execution Model Selected \n\r");
        execution_time = modal_decomposition_sw();
        //modal_decomposition_output ();
        
    }
    else
    {
        printf ("\n\rHW Execution Model Selected \n\r");
        execution_time = modal_decomposition_hw();
    }

    return execution_time;    
}


