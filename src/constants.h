// Simple file to contain program-wide constants
// Author: Jonathan Scanzi
// 28 May 2020
#pragma once

// Table of all relevant base addresses for all supported devices
#define AXI_BRIDGE_BASE_ARRIA_10        0xFF200000 
#define AXI_BRIDGE_BASE_STRATIX_10      0xF9000000

#define FPGA_RAM_BRIDGE_BASE_ARRIA_10   0xC0000000 //Base Address iWave
#define FPGA_RAM_BRIDGE_SIZE_ARRIA_10   (0xFBFFFFFF-(FPGA_RAM_BRIDGE_BASE_ARRIA_10)) //960MiB iWave
#define FPGA_RAM_BRIDGE_BASE_STRATIX_10 0x80000000
#define FPGA_RAM_BRIDGE_SIZE_STRATIX_10 0x40000000 // using 1GB of 2GB

// These are the only values that should be used by other files
#define AXI_BRIDGE_BASE AXI_BRIDGE_BASE_STRATIX_10
#define FPGA_RAM_BRIDGE_BASE FPGA_RAM_BRIDGE_BASE_STRATIX_10
#define FPGA_RAM_BRIDGE_SIZE FPGA_RAM_BRIDGE_SIZE_STRATIX_10