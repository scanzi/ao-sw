/*
*  C Implementation: swift execution controller
*
* Description: 
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

/**
 * @file sys_util.h
 * @author Andrea Guerrieri - Processor Architecture Laboratory <andrea.guerrieri@epfl.ch>
 * @date 9 Sep 2017
 * @version 0.1
 * @copyright 2017 See COPYING file that comes with this distribution
 * @brief This is the header file of sys_util.c module
 **/

#ifndef _AO_PARAMETERS_H
#define _AO_PARAMETERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdint.h>
#include <pthread.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <time.h>
#include <sched.h>       


#define INIT_STRING	"\n\r\n\r\
******************************************************************\n\r\
************** Adaptive Optics System ****************************\n\r\
************** EPFL-LAP 2019 - PlanetS ***************************\n\r\
******************************************************************\n\r\
";


#define IMAGE_SIZE  50
#define PUPIL_SIZE  IMAGE_SIZE/2
#define N_SIZE      (PUPIL_SIZE)*(PUPIL_SIZE)*2
#define D_SIZE      400
#define M_SIZE      400
#define A_SIZE      18
#define B_SIZE      18
#define C_SIZE      400


enum
{
    PREPROCESSING,
    MODAL_DECOMPOSITION,
    OPTIMAL_CONTROLS,
    ACTUATORS_COMMAND,
    ALL
};


typedef struct slopes_struct
{
    float s1;
    float s2;
}S_T;


//#define USE_FLOAT

#ifdef USE_FLOAT

extern float image[IMAGE_SIZE][IMAGE_SIZE];
extern float slopes[N_SIZE];
extern float d[N_SIZE][D_SIZE];
extern float r[D_SIZE][A_SIZE];
extern float m[M_SIZE][M_SIZE];
extern float a[A_SIZE];
extern float b[B_SIZE];
extern float c[C_SIZE];
extern float c_out[C_SIZE];

#else
extern int image[IMAGE_SIZE][IMAGE_SIZE];
extern int slopes[N_SIZE];
extern int d[N_SIZE][D_SIZE];
extern int r[D_SIZE][A_SIZE];
extern int m[M_SIZE][M_SIZE];
extern int a[A_SIZE];
extern int b[B_SIZE];
extern int c[C_SIZE];
extern int c_out[C_SIZE];

#endif

#define IMAGE_FPGA_MEM_OFF      0x0
#define IMAGE_FPGA_MEM_LEN      0x2000
#define S_FPGA_MEM_OFF          (IMAGE_FPGA_MEM_OFF+IMAGE_FPGA_MEM_LEN) //0x2000 - 0x2100
#define S_FPGA_MEM_LEN          0x100
#define R_FPGA_MEM_OFF          (S_FPGA_MEM_OFF+S_FPGA_MEM_LEN) //0x2100 -0x2300
#define R_FPGA_MEM_LEN          0x200
#define C_FPGA_MEM_OFF          (R_FPGA_MEM_OFF+R_FPGA_MEM_LEN) //0x2300 -0x2500
#define C_FPGA_MEM_LEN          0x200
#define CONTROLS_FPGA_MEM_OFF   (C_FPGA_MEM_OFF+C_FPGA_MEM_LEN) // 0x2500 - 0x2700
#define CONTROLS_FPGA_MEM_LEN   0x2000

//#define PERIOD               10 //100MHz
//#define PERIOD               5 //200MHz
#define PERIOD               4   //250MHz
//#define PERIOD               2.5 //400MHz


#endif
