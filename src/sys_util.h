/*
*  C Implementation: swift execution controller
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

/**
 * @file sys_util.h
 * @author Andrea Guerrieri - Processor Architecture Laboratory <andrea.guerrieri@epfl.ch>
 * @date 9 Sep 2017
 * @version 0.1
 * @copyright 2017 See COPYING file that comes with this distribution
 * @brief This is the header file of sys_util.c module
 **/

#ifndef _SYS_UTIL_H
#define _SYS_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdint.h>
#include <pthread.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <time.h>
#include <sched.h>
#include <vector>

#include "constants.h"

using namespace std;

enum
{
    FALSE,
    TRUE
};




#define SRC_ADDRESS  0x10000
//#define SRC_ADDRESS	PL_MEMORY_START
// #define DST_ADDRESS	(SRC_ADDRESS+(LENGTH))
// #define LENGTH 		(1024*1024*6)

#define	AXI_GP0_BASE			(AXI_BRIDGE_BASE)
//AXI Interrupt Controller

#define AIC_OFFS			0x00
// Example with 32 Accelerators
// #define AIC_EN_REG_0_OFFS		0x00
// #define AIC_EN_REG_1_OFFS		0x04
// #define AIC_INTERRUPTS_REG_0_OFFS	0x08
// #define AIC_INTERRUPTS_REG_1_OFFS	0x0C

// Example with 4 Accelerators
#define AIC_EN_REG_0_OFFS		0x00
#define AIC_INTERRUPTS_REG_0_OFFS	0x04

#define AIC_BASE                ( AXI_GP0_BASE + AIC_OFFS )
#define AIC_ENABLE_REG_0        ( AIC_BASE + AIC_EN_REG_0_OFFS)
#define AIC_ENABLE_REG_1	    ( AIC_BASE + AIC_EN_REG_1_OFFS )
#define AIC_INTERRUPTS_REG_0	( AIC_BASE + AIC_INTERRUPTS_REG_0_OFFS )
#define AIC_INTERRUPTS_REG_1	( AIC_BASE + AIC_INTERRUPTS_REG_1_OFFS )

#define AIC_REQ_BASE            ( AIC_BASE + AIC_REQ_OFFS )

#define AIC_REQ_TLB             ( AIC_BASE + AIC_REQ_TLB_OFFS )
#define AIC_REQ_PR              ( AIC_BASE + AIC_REQ_PR_OFFS )
#define AIC_REQ_PR_CODE         ( AIC_BASE + AIC_REQ_PR_CODE_OFFS )

#define AIC_REQ_TLB_MASK        0x000000FF
#define AIC_REQ_PR_MASK         0x0000FF00
#define AIC_REQ_PR_CODE_MASK    0x00FF0000

//AXI Accelerator Wrapper
#define AAW_OFFS                0x10000

#define AAW0_BASE	( AXI_GP0_BASE + AAW_OFFS )

//AXI Accelerator Wrapper Controller
#define AAWC_OFFS               0x04

#define AAW0C_BASE	( AAW0_BASE  )
#define AAW0C_STATUS	( AAW0_BASE  + AAWC_OFFS )

#define REQ_ISOL_EN         0x001
#define REQ_ISOL_DIS		0x002
#define AXI_LITE_ISOL_EN	0x004
#define AXI_LITE_ISOL_DIS	0x8
#define AXI_FULL_ISOL_EN	0x010
#define AXI_FULL_ISOL_DIS	0x020
#define IRQ_EN			0x040
#define IRQ_DIS			0x080
#define IRQ_ACK			0x100
#define RESET_TLB		0x400//0x200
#define RESET_PR		0x200//0x400

#define ISOLATE_VALUE		( REQ_ISOL_EN | AXI_LITE_ISOL_EN | AXI_FULL_ISOL_EN  )
#define DEISOLATE_VALUE		( REQ_ISOL_DIS | AXI_LITE_ISOL_DIS | AXI_FULL_ISOL_DIS  )

//AXI Accelerator Wrapper Programmable Region
#define ACC_OFFSET          0x8000

#define ACC_REG0_OFFSET 	0x00 //Source addresses
#define ACC_REG1_OFFSET		0x04 //Destination addresses
#define ACC_REG2_OFFSET		0x08 //Length 
#define ACC_REG3_OFFSET		0x0C //Burst len 
#define ACC_REG4_OFFSET		0x0C //Rotate amount

#define ACC_CTRL_REG_OFFSET	0x14

#define AAW0PR_BASE		( AAW0_BASE + ACC_OFFSET )
#define AAW0PR_CTRL		( AAW0_BASE + ACC_OFFSET  + ACC_CTRL_REG_OFFSET )

#define	ACC_START_VALUE		0x01

//AXI Accelerator Wrapper Translation Lookaside buffer
#define TLB_OFFSET		0x80
#define AAW0TLB_BASE	( AAW0_BASE + TLB_OFFSET )


void assign_cpu( int cpu_num );

void signal_handler ( void );
void termination_handler ( uint32_t sig );

void arguments_parser ( uint32_t argc,  char *argv[] );

void core_dump_init ( void );

long long get_timestamp( void );

void system_timer ( uint32_t timebase );

#define PL_MEMORY_START FPGA_RAM_BRIDGE_BASE 
#define PL_MEMORY_SIZE  FPGA_RAM_BRIDGE_SIZE


uint32_t fpga_ddr_memory_init ( uint32_t address, uint32_t size );
void hw_accelerators_init ();

uint32_t pl_write_file ( char *filename );
uint32_t pl_read_file ( char *filename );


extern volatile uint32_t *fpga_ddr_memory;


void read_configuration_file ( void );


#define FILE_PID ".aos.pid"

extern uint32_t bus_width;


uint32_t write_reg ( size_t address, uint32_t value );
uint32_t read_reg ( uint32_t address );
void read_reg_check ( uint32_t address,  uint32_t value );

long long current_timestamp( void );
long long getMicrotime( void );
uint32_t cycles_to_usec ( uint32_t cycles );


uint32_t hw_accelerator ( uint8_t region );


#define ARM_CPU_0	0
#define ARM_CPU_1	1

extern uint8_t application_run;

enum
{
    SW,
    HW
};


void split(const string& s, char c, vector<string>& v);

#endif
