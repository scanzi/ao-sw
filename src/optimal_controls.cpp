/*
*  C++ Implementation: preprocessing
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>

#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>

#include "sys_util.h"

#include "ao_parameters.h"
#include "optimal_controls.h"

uint32_t optimal_controls_sw ( )
{
    
    long long start_time, end_time;
    uint32_t elapsed_time;

    printf ("\n\rOptimal Controls Stage\n\r");  
        
    start_time = getMicrotime();

    for ( int indx = 0; indx < D_SIZE; indx++ )
    {
        if ( indx < 5 )
        {
            for (int k = 0; k < 18; k++ )
            {
                c[indx] += r[indx][k] * a[k] - r[indx][k] * b[k];   
            }            
        }
        else
        {
            c[indx] = r[indx][0] * a[0];
        }
    }
    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    printf ("SW Execution Time[us]: %d \n\r", elapsed_time);

    return elapsed_time;
}

#include "iir_regs.h"

uint32_t optimal_controls_hw (  )
{
    //hw_accelerator ( OPTIMAL_CONTROLS );
    
    #define OPTIMAL_CONTROLS_BASE_ADDRESS ((AXI_BRIDGE_BASE) + 0x00048000)

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS);
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF3_ADDR_REG_OFST, SRC_ADDRESS);


    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF_LEN_0_REG_OFST, 0x320032 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF_LEN_0_REG_OFST, 0x320032 );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF_LEN_1_REG_OFST, 0x320032 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_RD_BUFF_LEN_1_REG_OFST, 0x320032 );
    
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_RD_BUFF_DIR_REG_OFST, 0 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_RD_BUFF_DIR_REG_OFST, 0 );

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_RD_BURST_LEN_REG_OFST, 192 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_RD_BURST_LEN_REG_OFST, 192 );

    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+IIR_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS );

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_WR_BUFF_LEN_0_REG_OFST, 0x320032);
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_WR_BUFF_LEN_0_REG_OFST, 0x320032);

    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_WR_BUFF_LEN_1_REG_OFST, 0x320032 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_WR_BUFF_LEN_1_REG_OFST, 0x320032 );
    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_WR_BUFF_DIR_REG_OFST, 0 );
    read_reg_check ( OPTIMAL_CONTROLS_BASE_ADDRESS+  IIR_WR_BUFF_DIR_REG_OFST, 0 );

    
    write_reg (OPTIMAL_CONTROLS_BASE_ADDRESS+        IIR_CONTROL_REG_OFST, 0x3 );
    //read_reg_check (OPTIMAL_CONTROLS_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 168<<16 + 168 );
    
    uint32_t status_acc = 0;

    uint32_t status_acc2 = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ IIR_STATUS_REG_OFST );
    
    uint32_t cycles = 0;


    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

#define TIMEOUT     4
    uint32_t timeout_count = 0;
    
    do 
    {
        status_acc = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ IIR_STATUS_REG_OFST );
        //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
        //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
        cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ IIR_RUN_TIMER_REG_OFST );
        //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ MATVECT_RUN_TIMER_REG_OFST );

        //if ( timeout_count++ > TIMEOUT ) break;
            
        usleep (250000);
        printf ("waiting for ... status %x \n\r", status_acc); 
        printf ("runtime register %x \n\r", cycles); 


    }while ( !(status_acc & 0x01) );

    cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ IIR_OVERALL_DURATION_REG_OFST );

    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;
    //printf ("status %x \n\r", status_acc); 

     //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
     //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
     //cycles = read_reg ( OPTIMAL_CONTROLS_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
     printf ("cycles %d \n\r", cycles); 

     //return (cycles/1000*PERIOD);
     
     return (cycles_to_usec ( cycles ));
       
    
    //return elapsed_time;

    
    
    
}

uint32_t optimal_controls ( uint8_t mode )
{
    
    printf ("\n\rOptimal Controls Stage\n\r");  

    uint32_t execution_time;
    
    if ( mode == SW )
    {
        printf ("\n\rSW Execution Model Selected \n\r");
        execution_time = optimal_controls_sw();
        //modal_decomposition_output ();
        
    }
    else
    {
        printf ("\n\rHW Execution Model Selected \n\r");
        execution_time = optimal_controls_hw();
    }

    return execution_time;    
}

