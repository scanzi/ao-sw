/*
*  C++ Implementation: preprocessing
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>

#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>
#include <fstream>

#include "sys_util.h"

#include "ao_parameters.h"
#include "wfs_fe.h"




void parse_csv ( string strline )
{
    vector<string> v;
    
    static int offset;

    split( strline, ',', v);
    //printf ( "v.size() = %d ", v.size() );
    if ( v.size() > 0 )
    {
        for ( int indx = 0; indx < v.size(); indx++ )
        {
#ifdef USE_PL_MEMORY
            fpga_ddr_memory[IMAGE_MEM_ADD+indx+(offset*50)] = stoi(v[indx]);
            //printf ( "pl_memory_p[%d] = %d \n\r" , IMAGE_MEM_ADD+indx+(offset*50), stoi(v[indx]));
#else            
            int image_x = (indx+(offset*50)) %IMAGE_SIZE;
            int image_y = (indx+(offset*50)) /IMAGE_SIZE;
            
            //printf ( "image_x = %d, image_y = %d, val= %d \n\r", image_x, image_y, stoi(v[indx]));
#ifdef USE_FLOAT            
            image [image_x][image_y] = float (stoi(v[indx]));
#else
            image [image_x][image_y] = stoi(v[indx]);            
#endif
            
#endif
        }
    }
    offset++;

}
void read_wfs_image_from_file ( string filename )
{
    
    //string input_filename = FILENAME;
    ifstream inFile(filename);
    string strline;


    if (inFile.is_open()) 
    {
        while ( inFile )
        {
            getline(inFile, strline );
            
            parse_csv ( strline );

        }

        inFile.close();

        for ( int indx = 0; indx < IMAGE_SIZE; indx++ )
        {
            for ( int indx2 = 0; indx2 < IMAGE_SIZE; indx2++ )
            {
                #ifdef USE_PL_MEMORY
                image [indx][indx2] = fpga_ddr_memory[IMAGE_MEM_ADD+indx2+(indx*IMAGE_SIZE)];
                //image [indx][indx2] = float ( pl_memory_p[IMAGE_MEM_ADD+indx2+(indx*IMAGE_SIZE)] );
                //printf (" IMAGE_MEM_ADD+indx2+(indx*IMAGE_SIZE) = %d ",  IMAGE_MEM_ADD+indx2+(indx*IMAGE_SIZE)); 
                //printf (" image [%d][%d] = %f \n\r",  indx, indx2, image [indx][indx2] );
                #endif                
            }

        }
    }
    else
    {
        cout << "File " << filename << " not found "<< endl << endl<< endl;
        exit ( EXIT_FAILURE );
    }
    

}


#define CCD_IMAGE_FILENAME  "wfs_ccd.csv"

int image_expanded[240][240];

void expand_image ( int size )
{
    ofstream ccd_image;

    ccd_image.open (CCD_IMAGE_FILENAME);
    
    int a = size/2;
    int b = IMAGE_SIZE /2;
    
    int image_start = a - b;    
    int image_end = a + b;
    
    for (int indx = 0; indx < size; indx++) 
    {
        for (int indx2 = 0; indx2 < size; indx2++) 
        {

            if ( indx > image_start && indx2 > image_start && indx < image_end && indx2 < image_end )
            {
                ccd_image << image [indx-image_start][indx2-image_start];
                image_expanded[indx][indx] = image [indx-image_start][indx2-image_start];
            }
            else
            {
                ccd_image << "0";
                image_expanded[indx][indx] = 0;
            }
            if ( indx !=  size -1 )
            {
                ccd_image << ",";
            }
            if ( indx2 ==  size -1 )
            {
                ccd_image << endl;
            }
        }
    }
    
    ccd_image.close();

}

// void read_wfs_image_from_file ( )
// {
// //     FILE *fp;
// //     int val, indx;
// //     
// //     fp = fopen( FILENAME ,"r");
// //     
// //     indx = 0;
// //     
// //     if ( fp > 0 )
// //     {
// //         for ( int indx = 0; indx < IMAGE_SIZE; indx++ )
// //         {
// //             for ( int indx2 = 0; indx2 < IMAGE_SIZE; indx2++ )
// //             {            
// //                 fscanf(fp, "%d ", &val);
// //                 //printf ("%X\n\r", val );
// //                 pl_memory_p[IMAGE_MEM_ADD+indx2+(indx*IMAGE_SIZE)] = val;
// //                 //sleep (1);
// //             }
// //         }
// // 
// //     }
//     
//     parse_csv ( );
// 
// 
// }

int evaluate_block ( int size, int indx1, int indx2 )
{
    int block;
    int tmp_x, tmp_y;
    
    tmp_x = indx2/(size/4);
    tmp_y = indx1/(size/2);
    
    block = tmp_x;
    
    if ( tmp_y != 0 )
    {
        block = ( 7 - block); 
    }
    return block;
}

void fill_memory_image ( int size )
{
    
    printf ("\n\rCopy WFS Image into fpga's DDR memory");  

    int pixel_number;
    
    for (int indx = 0; indx < size; indx++) 
    {
        for (int indx2 = 0; indx2 < size; indx2++) 
        {

              int pixel_base = evaluate_block ( size, indx, indx2)+1;
              int block = evaluate_block ( size, indx, indx2);
              switch ( block )
              {
                  case 0:
                    //  pixel_number = (59 * 8 ) - ( indx2 * 8 ) + pixel_base + (indx*480);
                      pixel_number = pixel_base - 8*(indx2-((size/4)-1)) + size*2*(indx);
                    
                  break;
                  case 1:
                    //  pixel_number = pixel_base + ((indx2 % 60)*8);
                      pixel_number = pixel_base + 8*(indx2-((size/4))) + size*2*(indx);
                    
                  break;
                  case 2:
                      //pixel_number = (60 * 8 * (block+1) ) - ( (indx2+1) * 8 ) + pixel_base + (indx*480);
                    pixel_number = pixel_base - 8*(indx2-((3*size/4)-1)) + size*2*(indx);

                  break;
                  case 3:
                      //pixel_number = pixel_base + ((indx2 % 60)*8) + (indx*480);
                    pixel_number = pixel_base + 8*(indx2-(3*size/4)) + size*2*(indx);

                  break;
                  case 4:
                      //pixel_number = pixel_base + ((indx2 % 60)*8) - (indx*480);
                    //pixel_number = (59 * 8 ) - ( indx2 * 8 ) + pixel_base + (indx*480);
                    pixel_number = pixel_base + 8*(indx2-(3*size/4)) - size*2*(indx-(size-1));

                    break;
                  case 5:
                    pixel_number = pixel_base - 8*(indx2-((3*size/4)-1)) - size*2*(indx-(size-1));
                    
                    //  pixel_number = (240*240-2)-(8*(indx2-120))-480*(indx-120);
                      //pixel_number = (60 * 8 * (block+1) ) - ( (indx2+1) * 8 ) + pixel_base - (indx*480);
                        //pixel_number = (59 * 8 ) - ( indx2 * 8 ) + pixel_base + (indx*480);
                  break;
                  case 6:
                    //pixel_number = (59 * 8 ) - ( indx2 * 8 ) + pixel_base + (indx*480);
                    pixel_number = pixel_base + 8*(indx2-(size/4)) - size*2*(indx-(size-1));
                  break;
                  case 7:
                    pixel_number = pixel_base - 8*(indx2-((size/4)-1)) - size*2*(indx-(size-1));
                    //pixel_number = pixel_base - 8*(indx2-
                    //pixel_number = (60 * 8 * (block+1) ) - ( (indx2) * 8 ) + pixel_base - (indx*480);
                    //pixel_number = (120*480) - (59 * 8 ) - ( indx2 * 8 ) - ((indx-119)*480);
                    //pixel_number = (59 * 8 ) - ( indx2 * 8 ) + pixel_base + (indx*480);

                  break;
                  default:
                  break;
              }
              
            //printf ("y=%d, x=%d, block: %d , pixel_base %d pixel_number %d \n\r", indx, indx2, evaluate_block ( size, indx, indx2), pixel_base, pixel_number );
        //    printf (".");
#ifdef USE_PL_MEMORY
            fpga_ddr_memory[pixel_number] = image_expanded[indx][indx2];
#endif

        }
        printf (".");

    }
        printf ("Done\n\r");  

}

uint32_t wfs_get_image ( string filename )
{
    
    long long start_time, end_time;
    uint32_t elapsed_time;

    //printf ("\n\rWave Front Sensor Front-End Stage\n\r");  
        
    start_time = getMicrotime();
    
    read_wfs_image_from_file ( filename );
    
    expand_image ( 240 );
    
    fill_memory_image ( 240 );    
    
    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    //printf ("SW Execution Time[us]: %d \n\r", elapsed_time);

    return elapsed_time;
}

