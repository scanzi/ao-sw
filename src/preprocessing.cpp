/*
*  C++ Implementation: preprocessing
*
* Description:
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <iostream>

#include <pthread.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sched.h>

#include "sys_util.h"

#include "ao_parameters.h"
#include "preprocessing.h"

#define X_OFFSET_PUPIL_A    0
#define Y_OFFSET_PUPIL_A    0
#define X_OFFSET_PUPIL_B    PUPIL_SIZE
#define Y_OFFSET_PUPIL_B    0
#define X_OFFSET_PUPIL_C    0
#define Y_OFFSET_PUPIL_C    PUPIL_SIZE
#define X_OFFSET_PUPIL_D    PUPIL_SIZE
#define Y_OFFSET_PUPIL_D    PUPIL_SIZE

#define PA(indx_x, indx_y)  (image[indx_x+X_OFFSET_PUPIL_A][indx_y+Y_OFFSET_PUPIL_A])
#define PB(indx_x, indx_y)  (image[indx_x+X_OFFSET_PUPIL_B][indx_y+Y_OFFSET_PUPIL_B])
#define PC(indx_x, indx_y)  (image[indx_x+X_OFFSET_PUPIL_C][indx_y+Y_OFFSET_PUPIL_C])
#define PD(indx_x, indx_y)  (image[indx_x+X_OFFSET_PUPIL_D][indx_y+Y_OFFSET_PUPIL_D])

#define S1(indx_x, indx_y)  ((indx_x*PUPIL_SIZE)+indx_y)
#define S2(indx_x, indx_y)  ((indx_x*PUPIL_SIZE)+indx_y)+((PUPIL_SIZE)*(PUPIL_SIZE))


S_T slope_extraction ( float *pa, float *pb, float *pc, float *pd )
{
    
    S_T s;
    float sum;
    
    sum = *pa + *pb + *pc + *pd;
    
    //if ( sum != 0 )
    {
        s.s1 = *pa + *pb - *pc - *pd;
        s.s1 /= sum;

        s.s2 = -*pa + *pb - *pc + *pd;
        s.s2 /= sum;
    }    
    return s; 
    
}


uint32_t preprocessing_sw ( )
{
    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

    float sum;
    float s1,s2;
    
    for ( int indx_x = 0; indx_x < PUPIL_SIZE; indx_x++ )
    {
        for ( int indx_y = 0; indx_y < PUPIL_SIZE; indx_y++ )
        {
            //slopes [(indx_x*PUPIL_SIZE)+indx_y] = slope_extraction ( &PA(indx_x, indx_y), &PB(indx_x, indx_y), &PC(indx_x, indx_y), &PD(indx_x, indx_y));
            //S_T s = slope_extraction ( &PA(indx_x, indx_y), &PB(indx_x, indx_y), &PC(indx_x, indx_y), &PD(indx_x, indx_y));
            
            sum = PA(indx_x, indx_y) + PB(indx_x, indx_y) + PC(indx_x, indx_y) + PD(indx_x, indx_y) ;
            
            if ( sum != 0 )
            {
            
                s1 = PA(indx_x, indx_y) + PB(indx_x, indx_y) - PC(indx_x, indx_y) - PD(indx_x, indx_y) ;
                s2 = -PA(indx_x, indx_y) + PB(indx_x, indx_y) - PC(indx_x, indx_y) + PD(indx_x, indx_y) ;
                
                s1 /= sum;
                s2 /= sum;
            }            
            slopes [S1(indx_x, indx_y)] = s1;
            slopes [S2(indx_x, indx_y)] = s2;
        }
    }
        
    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;

    printf ("SW Execution Time[us]: %d \n\r", elapsed_time);
    
    return elapsed_time;
    
}

#include "preprocessing_regs.h"




#if 0 

0x000C: LINE_WORD_LEN: 0x00000001  en Asynchronous mode, doit etre 1
0x0010: RD_BUFFA_ADDR: 0x00002710
0x0014: RD_BUFFB_ADDR: 0x00002C50
0x0018: RD_BUFFC_ADDR: 0x00003190
0x001C: RD_BUFFD_ADDR: 0x000036D0
0x0020:  RD_BUFFX_LEN:   0x000000A8    chacun des buffers de lecture fait 168 words de 64 bits de long
0x0024:  BURST_LEN:         0x00000008  Taille des bursts de 8 ( sauf WrBuff0 et WrBuff2)
0x0028:  WR_BUFF0_ADDR:0x00004150
0x002C:  WR_BUFF1_ADDR:0x00004150
0x0030:  WR_BUFF2_ADDR: 0x00004BD0
0x0034:  WR_BUFF3_ADDR: 0x00004BD0
0x0038:  WR_BUFF_LEN_0:  0x00A800A8   chacun des buffers d écriture fait 168 words de 64 bits de long
0x003C:  WR_BUFF_LEN_1:  0x00A800A8
0x0040:  WR_BUFF_LEN_1:  0x00000005    les buffers d écriture 0 et 2 doivent être en Backward mode pour garder l order linéaire des données dans s1 et s2.

#endif

#define LINE_WORD_LEN 0x00000001 
#define RD_BUFFA_ADDR 0x00002710
#define RD_BUFFB_ADDR 0x00002C50
#define RD_BUFFC_ADDR 0x00003190
#define RD_BUFFD_ADDR 0x000036D0
#define RD_BUFFX_LEN  0x000000A8   
#define BURST_LEN	  0x00000010 //0x00000008
#define WR_BUFF0_ADDR 0x00004150
#define WR_BUFF1_ADDR 0x00004150
#define WR_BUFF2_ADDR 0x00004BD0
#define WR_BUFF3_ADDR 0x00004BD0
#define WR_BUFF_LEN_0 0x00A800A8   
#define WR_BUFF_LEN_1 0x00A800A8
#define WR_BUFF_DIR   0x00000005   
 
 
uint32_t preprocessing_hw (  )
{
    //hw_accelerator ( PREPROCESSING );
    
   
    #define PREPROCESSING_BASE_ADDRESS ((AXI_BRIDGE_BASE) + 0x00028000)
    //#define PREPROCESSING_BASE_ADDRESS 0xFF218000
    //printf ("src PL Memory address\n\r");  
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, LINE_WORD_LEN );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, LINE_WORD_LEN );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, 64 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_LINE_WORD_LEN_REG_OFST, 64 );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, SRC_ADDRESS );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, SRC_ADDRESS );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, SRC_ADDRESS+0x1000);
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, SRC_ADDRESS+0x1000 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, SRC_ADDRESS+0x2000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, SRC_ADDRESS+0x2000 );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFD_ADDR_REG_OFST, SRC_ADDRESS+0x3000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFD_ADDR_REG_OFST, SRC_ADDRESS+0x3000 );

//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, RD_BUFFA_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFA_ADDR_REG_OFST, RD_BUFFA_ADDR );
//     
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, RD_BUFFB_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFB_ADDR_REG_OFST, RD_BUFFB_ADDR );
// 
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, RD_BUFFC_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFC_ADDR_REG_OFST, RD_BUFFC_ADDR );
//     
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFD_ADDR_REG_OFST, RD_BUFFD_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFD_ADDR_REG_OFST, RD_BUFFD_ADDR );
// 
// 
     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFX_LEN_REG_OFST, RD_BUFFX_LEN );
     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_RD_BUFFX_LEN_REG_OFST, RD_BUFFX_LEN );
// 
//     
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, BURST_LEN );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, BURST_LEN );

     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x10 );
     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_BURST_LEN_REG_OFST, 0x10 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS+0x4000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, SRC_ADDRESS+0x4000 );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS+0x5000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, SRC_ADDRESS+0x5000 );

    write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS+0x6000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, SRC_ADDRESS+0x6000 );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS+0x7000 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, SRC_ADDRESS+0x7000 );

//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, WR_BUFF0_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF0_ADDR_REG_OFST, WR_BUFF0_ADDR );
//     
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, WR_BUFF1_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF1_ADDR_REG_OFST, WR_BUFF1_ADDR );
// 
//     write_reg (PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, WR_BUFF2_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+PREPROCESSING_WR_BUFF2_ADDR_REG_OFST, WR_BUFF2_ADDR );
//     
//     write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, WR_BUFF3_ADDR );
//     read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF3_ADDR_REG_OFST, WR_BUFF3_ADDR );
    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, WR_BUFF_LEN_0 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, WR_BUFF_LEN_0 );

    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, WR_BUFF_LEN_1 );
    read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, WR_BUFF_LEN_1 );
    
     write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_DIRECTION_REG_OFST, 0 ); //Forward
     read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF_DIRECTION_REG_OFST, 0 );
    
//    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_DIRECTION_REG_OFST, WR_BUFF_DIR ); //Backward
//    read_reg_check ( PREPROCESSING_BASE_ADDRESS+  PREPROCESSING_WR_BUFF_DIRECTION_REG_OFST, WR_BUFF_DIR ); 

    //write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, 0 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_0_REG_OFST, ((168<<16) + 168) );

    //write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 0 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, ((168<<16) + 168) );

    
    write_reg (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check (PREPROCESSING_BASE_ADDRESS+        PREPROCESSING_CONTROL_REG_OFST, 0x3 );
    //read_reg_check ( BASE_ADDRESS+  PREPROCESSING_WR_BUFF_LEN_1_REG_OFST, 168<<16 + 168 );
    
    uint32_t status_acc = 0;

    uint32_t status_acc2 = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_STATUS_REG_OFST );
    
    uint32_t cycles = 0;
    //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );


    long long start_time, end_time;
    uint32_t elapsed_time;
    start_time = getMicrotime();

#define TIMEOUT     4
    uint32_t timeout_count = 0;
    
    do 
    {
        status_acc = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_STATUS_REG_OFST );
        //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
        //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
        //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_RUN_TIMER_REG_OFST );
        

        //if ( timeout_count++ > TIMEOUT ) break;
            
        usleep (250000);
        printf ("waiting for ... status %x \n\r", status_acc); 
        //printf ("runtime register %x \n\r", cycles); 



    }while ( !(status_acc & 0x01) );
    
    cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );


    end_time = getMicrotime();
    
    elapsed_time = ( uint32_t ) ( end_time - start_time ) ;
    //printf ("status %x \n\r", status_acc); 

     //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_READ_DURATION_REG_OFST );
     //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_WRITE_DURATION_REG_OFST );
     //cycles = read_reg ( PREPROCESSING_BASE_ADDRESS+ PREPROCESSING_OVERALL_DURATION_REG_OFST );
    printf ("cycles %d \n\r", cycles); 


//     
     //return (cycles/1000*PERIOD);
     return cycles_to_usec ( cycles );  
    
    //return elapsed_time;

    
    
    
}

void preprocessing_output ( void )
{
#if 0
    printf( "slopes.s1: ");
    for ( int indx = 0; indx < K_SIZE; indx++ )
    {
        if ( indx != 0 )
        {
            printf( ", ");
        }
        printf( "%f ", slopes[indx].s1 );
    }
     printf( "\n\r" );

    
    printf( "slopes.s2: ");
    for ( int indx = 0; indx < K_SIZE; indx++ )
    {
        if ( indx != 0 )
        {
            printf( ", ");
        }
        printf( "%f ", slopes[indx].s2 );
    }
     printf( "\n\r" );
#endif

#if 0
    printf( "slopes: ");

    for ( int indx = 0; indx < N_SIZE; indx++ )
    {
        if ( indx != 0 )
        {
            printf( ", ");
        }
        
#ifdef USE_FLOAT
        printf( "%f ", slopes[indx] );
#else
        printf( "%d ", slopes[indx] );        
#endif
    }
    printf( "\n\r" );
    
#endif    
    for ( int indx = 0; indx < 2687; indx++ )
    {
        printf( "address: %x value : %x \n\r",  (indx*4), fpga_ddr_memory[indx] );
    }
}

uint32_t preprocessing ( uint8_t mode )
{

    printf ("\n\rPreprocessing Stage\n\r");
    uint32_t execution_time;
    
    if ( mode == SW )
    {
        printf ("\n\rSW Execution Model Selected \n\r");
        execution_time = preprocessing_sw ();
        //preprocessing_output ();
        
    }
    else
    {
        printf ("\n\rHW Execution Model Selected \n\r");
        execution_time = preprocessing_hw ();
    }

    //preprocessing_output ( );
   
    return execution_time;
}

