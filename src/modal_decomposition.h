/*
*  C Implementation: swift execution controller
*
* Description: 
*
*
* Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2017
*
* Copyright: See COPYING file that comes with this distribution
*
*/

/**
 * @file sys_util.h
 * @author Andrea Guerrieri - Processor Architecture Laboratory <andrea.guerrieri@epfl.ch>
 * @date 9 Sep 2017
 * @version 0.1
 * @copyright 2017 See COPYING file that comes with this distribution
 * @brief This is the header file of sys_util.c module
 **/

#ifndef _MODAL_H
#define _MODAL_H


uint32_t modal_decomposition ( uint8_t mode );

#endif
