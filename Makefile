# 
# Makefile : AOS application
# 
# Description: 
# This Makefile generates the Host Application binary for the ARM CortexA9
#  
# Rev. 0.1
# 
# Author: Andrea Guerrieri <andrea.guerrieri@epfl.ch (C) 2019
#
# Copyright: See COPYING file that comes with this distribution
#
# 

CROSS_COMPILE ?=

AS		= $(CROSS_COMPILE)as
#CC		= $(CROSS_COMPILE)gcc
CC		= $(CROSS_COMPILE)g++
CD		= cd
LD		= $(CC) -nostdlib
CPP		= $(CC) -E
AR		= $(CROSS_COMPILE)ar
NM		= $(CROSS_COMPILE)nm
STRIP		= $(CROSS_COMPILE)strip
OBJCOPY		= $(CROSS_COMPILE)objcopy
OBJDUMP		= $(CROSS_COMPILE)objdump
AWK		= awk
GENKSYMS	= scripts/genksyms/genksyms
DEPMOD		= /sbin/depmod
KALLSYMS	= scripts/kallsyms
PERL		= perl
CHECK		= sparse
DOXYGEN		= doxygen


APP ?= aos

SRCDIR=./src
OBJDIR=./src
BINDIR=./bin
SRCDIR=./src
DOCSDIR=./docs

#CC=g++
#CFLAGS=-O3 -Wall
#CFLAGS=-O0 -g -Wall
#CFLAGS=pthread -lm -O0 -g -static-libgcc -Wall -static -fpermissive
DEFINE1=-DARM_PROCESSOR
DEFINE2=-DUSE_PL_MEMORY
DEFINE3=-DUSE_FLOAT
#DEFINE2=-D_USE_TCP
DEFINES=${DEFINE1} ${DEFINE2} ${DEFINE3}

# ifdef $(CROSS_COMPILE)
# 	CFLAGS=-O3 -g -mcpu=cortex-a9 -Wall -fpermissive -mfloat-abi=hard -mfpu=neon-fp16 $(DEFINES) 
# #-mcpu=cortex-a9  -mfloat-abi=hard -mfpu=neon-fp16
# else
# 	CFLAGS=-O3 -g -Wall -fpermissive -march=x86-64
# endif


ifeq ($(CC),g++)
	CFLAGS=-Ofast -pg -g -Wall -fpermissive -march=x86-64
else
	CFLAGS=-Ofast -g -mcpu=cortex-a53 -Wall -fpermissive  $(DEFINES) 
endif

LFLAGS=-lpthread -lm -static-libgcc -static-libstdc++


IDIR=$(SRCDIR)



$(BINDIR)/$(APP) :: $(SRCDIR)/wfs_fe.o $(SRCDIR)/preprocessing.o $(SRCDIR)/modal_decomposition.o $(SRCDIR)/optimal_controls.o $(SRCDIR)/actuators_command.o $(SRCDIR)/dm_fe.o \
			$(SRCDIR)/aos.o $(SRCDIR)/sys_util.o 
		    $(CC) $(CFLAGS) $? -o $@ $(LDIR) $(LFLAGS)
	

$(SRCDIR)/aos.o :: $(SRCDIR)/aos.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/wfs_fe.o :: $(SRCDIR)/wfs_fe.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/preprocessing.o :: $(SRCDIR)/preprocessing.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/modal_decomposition.o :: $(SRCDIR)/modal_decomposition.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/optimal_controls.o :: $(SRCDIR)/optimal_controls.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/actuators_command.o :: $(SRCDIR)/actuators_command.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/dm_fe.o :: $(SRCDIR)/dm_fe.cpp
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

$(SRCDIR)/sys_util.o :: $(SRCDIR)/sys_util.c
	$(CC) $(CFLAGS) -c $? -o $@ -I $(IDIR) -I $(SRCDIR)

clean ::
	#rm -rf $(BINDIR)/* $(OBJDIR)/*.o $(SRCDIR)/*.o
	rm -rf $(OBJDIR)/*.o $(SRCDIR)/*.o
